<?php

/**
 * @file
 * Contains \Drupal\user_ip\UserIpSubscriber.
 */

namespace Drupal\user_ip\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class UserIpSubscriber.
 *
 * @package Drupal\user_ip
 */
class UserIpSubscriber implements EventSubscriberInterface {

  /**
   * Constructor.
   */
  public function __construct() {

  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    $events['kernel.request'] = ['getGeoIp', 200];
    return $events;
  }

  /**
   * This method is called whenever the kernel.request event is
   * dispatched.
   *
   * @param GetResponseEvent $event
   */

  public function getGeoIp(Event $event) {
    if((\Drupal::currentUser()->isAuthenticated()) && !isset($_SESSION['freegeoip']) ) {
      $curl = curl_init();
      $url_opt = 'http://freegeoip.net/json/'. $_SERVER['REMOTE_ADDR'];
      curl_setopt_array($curl, array(
        CURLOPT_URL => $url_opt,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "cache-control: no-cache",
        ),
      ));
      $_SESSION['freegeoip'] = curl_exec($curl);
    }
  }

}
